flasher
=======

Simple selectable-colour LED flasher for USAR training/response

The flasher circuit is designed to fit inside a clear 35mm film canister.
It has three power states -- off, flashing (~100ms flash, ~1900ms off) and
on -- and four selectable LED colours -- yellow, red, green and blue. With a
300mAh battery, it should last 10 hours with the LED on and 200 hours
(~1 week) with the LED flashing.

At time of design, the board cost around $25NZD (sourcing PCBs from [JLPCB][],
components from [Mouser][] and the battery from [AliExpress]).
